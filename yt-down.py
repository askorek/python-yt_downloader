#usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 11:15:52 2015

@author: adam.skorek
"""

from xml.dom import minidom
import os
import logging

###############################################################################
# configuration
USER = 'askorek'
DOWNLOADED_LIST = '/home/pi/yt/downloaded.txt'
LOG_FILE = '/home/pi/yt/log.txt'

###############################################################################
logging.basicConfig(filename=LOG_FILE,level=logging.INFO)
logger = logging.getLogger('YT down')
hdlr = logging.FileHandler('log.txt')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)


logger.info("Script started, searching for new movies")

os.system("python yt_subs_down.py " + USER + " subs.xml")

with open("subs.xml") as f:
    xml_content = minidom.parse(f)
cNodes = xml_content.childNodes
items = cNodes[0].getElementsByTagName("item")

with open(DOWNLOADED_LIST, 'r') as d:
    already_downloaded = d.readlines()


for item in items:
    link = item.childNodes[3].firstChild.nodeValue
    if not str(link + '\n') in already_downloaded:
        logger.info("Starting to download: " + str(link))
        res = os.system('/usr/local/bin/youtube-dl -f 18 -r 500K --write-sub -o "/media/pi/pi-media/6. YT-down/%(uploader)s/%(upload_date)s - %(title)s.%(ext)s" ' + str(link))
        if res == 0:
            logger.info("File successfully downloaded")
            with open(DOWNLOADED_LIST, 'a') as d:
                d.write(link + '\n')
        else:
            logger.warning("Error downloading file")
               
os.remove("subs.xml")
